import logging
from src.env_config import FLASK_ENV, SECRET_KEY, DB_HOST, DB_NAME, DB_USER, DB_PASSWORD

# Flask
SECRET_KEY = SECRET_KEY

# SQLAlchemy
SQLALCHEMY_DATABASE_URI = f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Logging
if FLASK_ENV == 'development':
    LOG_LEVEL = logging.DEBUG
else:
    LOG_LEVEL = logging.INFO
