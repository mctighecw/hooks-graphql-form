import os
from pathlib import Path
from dotenv import load_dotenv

FLASK_ENV = os.getenv('FLASK_ENV')

if FLASK_ENV == 'development':
    # project root directory
    root_dir = Path(__file__).parent.parent.parent
    env_file = os.path.join(root_dir, '.env.dev')
    load_dotenv(env_file)

SECRET_KEY = os.getenv('SECRET_KEY')
DB_HOST = os.getenv('DB_HOST')
DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
