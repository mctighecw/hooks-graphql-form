from datetime import datetime, timezone
import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField

from src.models import User
from src.database import db


# general
class UserInput(graphene.InputObjectType):
    """
    GraphQL input types
    """
    id = graphene.Int()
    username = graphene.String()
    first_name = graphene.String()
    last_name = graphene.String()
    email = graphene.String()


class UserType(SQLAlchemyObjectType):
    class Meta:
        model = User


# queries
class Query(graphene.ObjectType):
    """
    All GraphQL queries
    """
    hello = graphene.String()
    users = graphene.List(UserType)
    user = graphene.Field(UserType, id=graphene.Int())

    def resolve_hello(self, info):
        return 'Hello world!'

    def resolve_users(self, info):
        query = UserType.get_query(info)
        return query.all()

    def resolve_user(self, info, id):
        query = UserType.get_query(info)
        return query.filter(User.id == id).first()


# mutations
class AddUser(graphene.Mutation):
    """
    GraphQL mutation to add a new user
    """
    class Arguments:
        username = graphene.String()
        first_name = graphene.String()
        last_name = graphene.String()
        email = graphene.String()

    user = graphene.Field(lambda: UserType)

    def mutate(self, info, username, first_name, last_name, email):
        user = User(username=username, first_name=first_name,
                    last_name=last_name, email=email)

        db.session.add(user)
        db.session.commit()

        return AddUser(user=user)


class UpdateUser(graphene.Mutation):
    """
    GraphQL mutation to update an existing user
    """
    class Arguments:
        id = graphene.Int()
        username = graphene.String()
        first_name = graphene.String()
        last_name = graphene.String()
        email = graphene.String()
        updated_at = graphene.DateTime()

    user = graphene.Field(lambda: UserType)

    def mutate(self, info, **kwargs):
        id = kwargs.get('id')
        user = db.session.query(User).filter(User.id == id).first()

        utc_dt = datetime.now(timezone.utc)
        local_dt = utc_dt.astimezone()

        for k, v in kwargs.items():
            if k != 'id':
                setattr(user, k, v)

            setattr(user, 'updated_at', local_dt)
            db.session.commit()

        return UpdateUser(user=user)


class DeleteUser(graphene.Mutation):
    """
    GraphQL mutation to delete a user
    """
    class Arguments:
        id = graphene.Int()

    user = graphene.Field(lambda: UserType)

    def mutate(self, info, id):
        user = db.session.query(User).filter(User.id == id).first()
        db.session.delete(user)
        db.session.commit()

        return DeleteUser(user=user)


class Mutation(graphene.ObjectType):
    """
    All GraphQL mutations
    """
    addUser = AddUser.Field()
    updateUser = UpdateUser.Field()
    deleteUser = DeleteUser.Field()


schema = graphene.Schema(query=Query, mutation=Mutation, auto_camelcase=False)
