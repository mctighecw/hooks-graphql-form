import logging
import os

from flask import Flask
from flask_cors import CORS
from flask_graphql import GraphQLView

from src.graphql.schema import schema
from src.database import db
from src import flask_config


def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)

    # environment
    FLASK_ENV = os.getenv('FLASK_ENV')

    # allow any origin for development environment
    if FLASK_ENV == 'development':
        CORS(app)

    # configure logger
    logging.basicConfig(format='%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s',
                        level=app.config['LOG_LEVEL'])

    # init db connection
    db.init_app(app)

    # GraphQL
    if FLASK_ENV == 'development':
        allow_graphiql = True
    else:
        allow_graphiql = False

    app.add_url_rule('/graphql',
                    view_func=GraphQLView.as_view(
                        'graphql',
                        schema=schema,
                        graphiql=allow_graphiql)
                    )

    return app
