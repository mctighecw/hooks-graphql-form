from datetime import datetime, timezone
from src.database import db

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(40), unique=True)
    first_name = db.Column(db.String(40))
    last_name = db.Column(db.String(40))
    email = db.Column(db.String(40), unique=True)
    created_at = db.Column(db.DateTime, default=datetime.now(timezone.utc).astimezone())
    updated_at = db.Column(db.DateTime)
