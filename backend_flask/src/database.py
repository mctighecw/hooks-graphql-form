from flask_sqlalchemy import SQLAlchemy

# This is the singleton that will be used in order to access
# the database from the rest of the application
db = SQLAlchemy()
