#!/bin/sh

echo Initializing database in Docker container...

docker exec -it hooks-graphql-form_postgresql_1 /bin/bash /usr/src/scripts/create_db_prod.sh
