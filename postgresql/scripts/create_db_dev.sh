#!/usr/bin/env bash

echo Creating new development database...

if [ "$( psql $USER -tAc "SELECT 1 FROM pg_roles WHERE rolname='hgf_user'" )" != '1' ]
then
  echo Creating user: hgf_user. Please enter password "\"mypassword"\"
  sudo -u $USER createuser -P hgf_user
  sudo -u $USER createdb -O hgf_user -E 'utf8' -T 'template0' hgf_db
fi

echo Closing any open db connections...
sudo -u $USER psql postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'hgf_db' AND pid <> pg_backend_pid();"

echo Deleting old db...
sudo -u $USER dropdb hgf_db

echo Creating new db...
sudo -u $USER createdb -O hgf_user -E 'utf8' -T 'template0' hgf_db

psql -U hgf_user -d hgf_db -a -f ./sql/db.sql
psql -U hgf_user -d hgf_db -a -f ./sql/seeds.sql
