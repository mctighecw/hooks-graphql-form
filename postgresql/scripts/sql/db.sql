CREATE TABLE users (
  id serial PRIMARY KEY,
  username varchar(40) NOT NULL,
  first_name varchar(40) NOT NULL,
  last_name varchar(40) NOT NULL,
  email varchar(40) NOT NULL,
  created_at timestamptz DEFAULT NOW(),
  updated_at timestamptz
);
