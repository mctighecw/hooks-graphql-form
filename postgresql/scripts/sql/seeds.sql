INSERT INTO users (username, first_name, last_name, email)
VALUES
  ('jsmith', 'John', 'Smith', 'john@smith.com'),
  ('sjones', 'Susan', 'Jones', 'susan@jones.com'),
  ('jwilson', 'James', 'Wilson', 'james@wilson.com');
