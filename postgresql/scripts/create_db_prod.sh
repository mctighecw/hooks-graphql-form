#!/usr/bin/env bash

echo Setting up $ENVIRONMENT database...

echo Creating user: $DB_USER. Please enter password: $DB_PASSWORD
createuser -U postgres -P $DB_USER

echo Creating new db...
createdb -U postgres -E 'utf8' -T 'template0' $DB_NAME

psql -U $DB_USER -d $DB_NAME -a -f /usr/src/scripts/sql/db.sql
psql -U $DB_USER -d $DB_NAME -a -f /usr/src/scripts/sql/seeds.sql
