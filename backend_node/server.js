const express = require('express');
const cors = require('cors');
const { ApolloServer } = require('apollo-server-express');
const { schema } = require('./graphql/schema');

const isDev = process.env.ENVIRONMENT === 'development';

const server = new ApolloServer({
  schema,
  introspection: isDev,
  playground: isDev,
});

const app = express();
const port = 4000;

app.use(cors());
server.applyMiddleware({ app });

app.listen({ port }, () => {
  console.log(`GraphQL Express server running on port ${port} at ${server.graphqlPath}`);
});
