const typeDefs = `
  scalar DateTime
  type User {
    id: ID
    username: String
    first_name: String
    last_name: String
    email: String
    created_at: DateTime
    updated_at: DateTime
  }
  type Query {
    hello: String
    users: [User]
    user(id: ID): User
  }
  type Mutation {
    addUser(username: String, first_name: String, last_name: String, email: String): User
    updateUser(id: ID, username: String, first_name: String, last_name: String, email: String): User
    deleteUser(id: ID): User
  }
`;

exports.typeDefs = typeDefs;
