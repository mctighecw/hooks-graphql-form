const pgPromise = require('pg-promise');
const pgp = pgPromise({});

require('dotenv').config({ path: '../.env.dev' });

const { DB_HOST, DB_NAME, DB_USER, DB_PASSWORD } = process.env;

const config = {
  host: DB_HOST,
  port: 5432,
  database: DB_NAME,
  user: DB_USER,
  password: DB_PASSWORD,
};

const db = pgp(config);

exports.db = db;
