const { GraphQLScalarType } = require('graphql');
const moment = require('moment');
const { db } = require('./db');

const resolvers = {
  DateTime: new GraphQLScalarType({
    name: 'DateTime',
    description: 'A date and time, represented as an ISO-8601 string',
    serialize: (value) => value.toISOString(),
    parseValue: (value) => new Date(value),
    parseLiteral: (ast) => new Date(ast.value)
  }),
  Query: {
    hello(obj, args, context, info) {
      return 'Hello world!';
    },
    users(obj, args, context, info) {
      const query = 'SELECT * FROM users';
      return db.manyOrNone(query);
    },
    user(obj, { id }, context, info) {
      const query = `SELECT * FROM users WHERE id=$1`;
      const values = [id];
      return db.one(query, values);
    },
  },
  Mutation: {
    addUser: (_, { username, first_name, last_name, email }, context, info) => {
      const query = `INSERT INTO users (username, first_name, last_name, email) VALUES ($1, $2, $3, $4) RETURNING username`;
      const values = [username, first_name, last_name, email];
      return db.one(query, values);
    },
    updateUser: (_, args, context, info) => {
      const keys = Object.keys(args);
      const values = Object.values(args);
      const now = moment();
      const updateTime = now.format('YYYY-MM-DD HH:mm:ss.SSS Z');
      const params = [];

      // update only the fields received
      for (let i = 1; i < keys.length; i++) {
        params.push(`${keys[i]}=$${i + 1}`);
      }

      // add updated_at timestamp
      params.push(`updated_at=$${keys.length + 1}`);
      values.push(updateTime);

      const query = `UPDATE users SET ${params.join(', ')} WHERE id=$1 RETURNING username`;
      return db.one(query, values);
    },
    deleteUser: (_, { id }, context, info) => {
      const query = `DELETE FROM users WHERE id=$1 RETURNING id`;
      const values = [id];
      return db.one(query, values);
    },
  },
};

exports.resolvers = resolvers;
