#!/bin/sh

echo Building Docker images and starting containers...

docker-compose up -d --build
