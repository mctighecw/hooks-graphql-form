import { gql } from 'apollo-boost';

const { BACKEND_TYPE } = process.env;

export let ADD_NEW_USER;
export let UPDATE_USER;
export let DELETE_USER;

if (BACKEND_TYPE === 'flask') {
  // Flask backend (return full user object; id is Int!)
  ADD_NEW_USER = gql`
    mutation addUser($username: String!, $first_name: String!, $last_name: String!, $email: String!) {
      addUser(username: $username, first_name: $first_name, last_name: $last_name, email: $email) {
        user {
          id
          username
          first_name
          last_name
          email
        }
      }
    }
  `;

  UPDATE_USER = gql`
    mutation updateUser(
      $id: Int!
      $username: String!
      $first_name: String!
      $last_name: String!
      $email: String!
    ) {
      updateUser(
        id: $id
        username: $username
        first_name: $first_name
        last_name: $last_name
        email: $email
      ) {
        user {
          username
        }
      }
    }
  `;

  DELETE_USER = gql`
    mutation deleteUser($id: Int!) {
      deleteUser(id: $id) {
        user {
          username
        }
      }
    }
  `;
} else {
  // Node backend (return one user field; id is ID!)
  ADD_NEW_USER = gql`
    mutation addUser($username: String!, $first_name: String!, $last_name: String!, $email: String!) {
      addUser(username: $username, first_name: $first_name, last_name: $last_name, email: $email) {
        username
      }
    }
  `;

  UPDATE_USER = gql`
    mutation updateUser(
      $id: ID!
      $username: String!
      $first_name: String!
      $last_name: String!
      $email: String!
    ) {
      updateUser(
        id: $id
        username: $username
        first_name: $first_name
        last_name: $last_name
        email: $email
      ) {
        username
      }
    }
  `;

  DELETE_USER = gql`
    mutation deleteUser($id: ID!) {
      deleteUser(id: $id) {
        id
      }
    }
  `;
}
