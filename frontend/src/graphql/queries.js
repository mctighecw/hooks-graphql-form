import { gql } from 'apollo-boost';

export const GET_ALL_USERS = gql`
  {
    users {
      id
      username
      first_name
      last_name
      email
    }
  }
`;
