import React, { useState } from 'react';
import Button from '../shared/Button/Button';
import Form from '../Form/Form';
import List from '../List/List';
import Footer from '../Footer/Footer';

import reactLogo from '../../assets/react-logo.svg';
import graphqlLogo from '../../assets/graphql-logo.svg';
import './styles.less';

const Main = () => {
  const [showComponent, setShowComponent] = useState('');

  return (
    <div className="main">
      <div className="logos-header">
        <img src={reactLogo} alt="" />
        <img src={graphqlLogo} alt="" />

        <div className="header">React Hooks & GraphQL</div>
      </div>

      <div className="content">
        <div className="main-buttons">
          <Button label="Add new user" onClickMethod={() => setShowComponent('newUser')} />

          <Button label="View user list" onClickMethod={() => setShowComponent('userList')} />
        </div>

        {showComponent === 'newUser' && <Form />}
        {showComponent === 'userList' && <List />}
      </div>

      {showComponent === '' &&
        <Footer />
      }
    </div>
  );
};

export default Main;
