import React from 'react';
import './styles.less';

const Footer = () => (
  <div className="footer">
    <div className="copyright">
      <div className="line">&copy; 2019 Christian McTighe.</div>
      <div className="line">Coded by Hand.</div>
    </div>
  </div>
);

export default Footer;
