import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { ADD_NEW_USER } from '../../graphql/mutations';
import { GET_ALL_USERS } from '../../graphql/queries';

import Input from '../shared/Input/Input';
import Button from '../shared/Button/Button';
import './styles.less';

const Form = () => {
  const [userName, setUsername] = useState('');
  const [firstName, setFirstname] = useState('');
  const [lastName, setLastname] = useState('');
  const [eMail, setEmail] = useState('');
  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');

  const handleUserNameInput = (e) => {
    setUsername(e.target.value);
  };

  const handleFirstNameInput = (e) => {
    setFirstname(e.target.value);
  };

  const handleLastNameInput = (e) => {
    setLastname(e.target.value);
  };

  const handleEmailInput = (e) => {
    setEmail(e.target.value);
  };

  const onCompleteAddUser = (data) => {
    clearState();
    setSuccess('User has been added');
  };

  const clearState = () => {
    setUsername('');
    setFirstname('');
    setLastname('');
    setEmail('');
    setError('');
    setSuccess('');
  };

  const sendDisabled =
    userName.length === 0 || firstName.length === 0 || lastName.length === 0 || eMail.length === 0;

  return (
    <div className="form-container">
      <div className="form-heading">Add User</div>

      <div className="form">
        <Input value={userName} name="UserName" placeholder="Username" onChangeMethod={handleUserNameInput} />

        <Input
          value={firstName}
          name="FirstName"
          placeholder="First name"
          onChangeMethod={handleFirstNameInput}
        />

        <Input
          value={lastName}
          name="LastName"
          placeholder="Last name"
          onChangeMethod={handleLastNameInput}
        />

        <Input value={eMail} name="eMail" placeholder="Email" onChangeMethod={handleEmailInput} />

        <div className="message">
          <div className="error">{error}</div>
          <div className="success">{success}</div>
        </div>

        <div className="buttons">
          <Mutation
            mutation={ADD_NEW_USER}
            variables={{
              username: userName,
              first_name: firstName,
              last_name: lastName,
              email: eMail,
            }}
            onCompleted={(data) => onCompleteAddUser(data)}
            refetchQueries={() => {
              return [
                {
                  query: GET_ALL_USERS,
                },
              ];
            }}
          >
            {(addUser) => <Button label="Send" disabled={sendDisabled} onClickMethod={addUser} />}
          </Mutation>

          <Button label="Clear" onClickMethod={clearState} />
        </div>
      </div>
    </div>
  );
};

export default Form;
