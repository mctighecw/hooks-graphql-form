import React from 'react';
import './styles.less';

const Input = ({ placeholder, name, value, disabled, onChangeMethod }) => (
  <input
    type="text"
    placeholder={placeholder}
    name={name}
    value={value}
    disabled={disabled}
    onChange={onChangeMethod}
    className="input"
  />
);

export default Input;
