import React from 'react';
import './styles.less';

const EditField = ({ placeholder, name, value, disabled, onChangeMethod }) => (
  <input
    type="text"
    placeholder={placeholder}
    name={name}
    value={value}
    disabled={disabled}
    onChange={onChangeMethod}
    className="edit-field"
  />
);

export default EditField;
