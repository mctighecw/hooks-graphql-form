import React from 'react';
import './styles.less';

const Button = ({ label, disabled, onClickMethod }) => (
  <button className="button" disabled={disabled || false} onClick={onClickMethod}>
    {label}
  </button>
);

export default Button;
