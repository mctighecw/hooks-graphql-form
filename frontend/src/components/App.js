import React from 'react';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import Main from './Main/Main';

const prefix = process.env.NODE_ENV === 'development' ? 'http://localhost:4000' : '';

const client = new ApolloClient({
  uri: `${prefix}/graphql`,
});

const App = () => (
  <ApolloProvider client={client}>
    <Main />
  </ApolloProvider>
);

export default App;
