import React, { useState } from 'react';
import { Query, Mutation } from 'react-apollo';
import { GET_ALL_USERS } from '../../graphql/queries';
import { UPDATE_USER, DELETE_USER } from '../../graphql/mutations';
import EditField from '../shared/EditField/EditField';

const Users = () => {
  const initialState = { id: '', username: '', first_name: '', last_name: '', email: '' };
  const [userInfo, setUserInfo] = useState(initialState);

  const renderMessage = (type) => (
    <tr>
      <td colSpan="6">{type}</td>
    </tr>
  );

  const handleUpdateField = (event) => {
    const { name, value } = event.target;
    setUserInfo({ ...userInfo, [name]: value });
  };

  const handleUpdateUser = (updateUser) => {
    let emptyFields = [];

    for (let key in userInfo) {
      if (userInfo[key] === '') {
        emptyFields.push(key);
      }
    }

    if (emptyFields.length > 0) {
      alert('Please fill in empty fields before saving!');
    } else {
      updateUser();
      setUserInfo(initialState);
    }
  };

  const handleDeleteUser = (deleteUser) => {
    const yes = confirm('Are you sure that you want to delete this user?');
    if (yes) deleteUser();
  };

  return (
    <Query query={GET_ALL_USERS} onCompleted={(data) => console.log(data)}>
      {({ loading, error, data }) => {
        if (loading) return renderMessage('Loading...');
        if (error) return renderMessage('Error');

        const success =
          Object.keys(data) && Object.keys(data.users) && Object.keys(data.users).length > 0
            ? Object.values(data.users).map((item, index) =>
                userInfo === null || (userInfo !== null && userInfo.id !== item.id) ? (
                  <tr key={index}>
                    <td>{item.username}</td>
                    <td>{item.first_name}</td>
                    <td>{item.last_name}</td>
                    <td>{item.email}</td>
                    <td
                      className="edit"
                      onClick={() =>
                        setUserInfo({
                          id: item.id,
                          username: item.username,
                          first_name: item.first_name,
                          last_name: item.last_name,
                          email: item.email,
                        })
                      }
                    >
                      Edit
                    </td>
                    <Mutation
                      mutation={DELETE_USER}
                      variables={{ id: item.id }}
                      onCompleted={(data) => console.log(data)}
                      refetchQueries={() => {
                        return [
                          {
                            query: GET_ALL_USERS,
                          },
                        ];
                      }}
                    >
                      {(deleteUser) => (
                        <td className="delete" onClick={() => handleDeleteUser(deleteUser)}>
                          Delete
                        </td>
                      )}
                    </Mutation>
                  </tr>
                ) : (
                  <tr key={index}>
                    <td className="edit-td">
                      <EditField
                        value={userInfo.username}
                        name="username"
                        placeholder="Username"
                        onChangeMethod={(event) => handleUpdateField(event)}
                      />
                    </td>
                    <td className="edit-td">
                      <EditField
                        value={userInfo.first_name}
                        name="first_name"
                        placeholder="First name"
                        onChangeMethod={(event) => handleUpdateField(event)}
                      />
                    </td>
                    <td className="edit-td">
                      <EditField
                        value={userInfo.last_name}
                        name="last_name"
                        placeholder="Last name"
                        onChangeMethod={(event) => handleUpdateField(event)}
                      />
                    </td>
                    <td className="edit-td">
                      <EditField
                        value={userInfo.email}
                        name="email"
                        placeholder="Email"
                        onChangeMethod={(event) => handleUpdateField(event)}
                      />
                    </td>
                    <td className="edit" onClick={() => setUserInfo(initialState)}>
                      Cancel
                    </td>
                    <Mutation
                      mutation={UPDATE_USER}
                      variables={userInfo}
                      onCompleted={(data) => console.log(data)}
                      refetchQueries={() => {
                        return [
                          {
                            query: GET_ALL_USERS,
                          },
                        ];
                      }}
                    >
                      {(updateUser) => (
                        <td className="delete" onClick={() => handleUpdateUser(updateUser)}>
                          Save
                        </td>
                      )}
                    </Mutation>
                  </tr>
                )
              )
            : renderMessage('No users');

        return success;
      }}
    </Query>
  );
};

export default Users;
