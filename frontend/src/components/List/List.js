import React from 'react';
import Users from '../Users/Users';
import './styles.less';

const List = () => (
  <div className="list">
    <div className="list-heading">User List</div>

    <table>
      <tbody>
        <tr>
          <th>Username</th>
          <th>First name</th>
          <th>Last name</th>
          <th>Email</th>
          <th colSpan="2">Actions</th>
        </tr>

        <Users />
      </tbody>
    </table>
  </div>
);

export default List;
