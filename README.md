# README

A project to try out React hooks and GraphQL.

## App Information

App Name: hooks-graphql-form

Created: February 2019; updated July 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/hooks-graphql-form)

## Tech Stack

- Frontend

  - React (including hooks)
  - Webpack
  - Less
  - GraphQL
  - Apollo
  - Prettier

- Node backend

  - Node.js
  - Apollo Express server

- Flask backend

  - Flask
  - SQLAlchemy
  - Graphene

- Database
  - PostgreSQL

## To Set Up and Run

Add `.env.dev` and `.env.prod` files to root directory

1. Run from terminal

  - Set up PostgreSQL database

    ```
    $ cd postgresql/scripts
    $ source create_db_dev.sh
    ```

  - _Node backend_: install Node modules and start

    ```
    $ cd backend_node
    $ npm install
    $ npm start
    ```

    **OR**

  - _Flask backend_: install Python packages and start

    ```
    $ cd backend_flask
    $ virtualenv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt
    $ FLASK_ENV="development" python run.py
    ```

  - Set up frontend

    - Set 'node' or 'flask' variable in `frontend/webpack.config.js`
    - Install Node modules and start frontend

    ```
    $ cd frontend
    $ npm install
    $ npm start
    ```

  - Go to `localhost:3000`
  - GraphQL web interface available at `localhost:4000/graphql`

2. Run via Docker

  - Set 'node' or 'flask' variable in `frontend/webpack.config.prod.js`
  - Build containers and initialize database

    ```
    $ source run_build.sh
    $ source docker_db_init.sh
    ```

  - Go to `localhost:80`

Last updated: 2024-11-29
